import argparse
import asyncio
import heapq

import aioconsole


class ElevatorSimulator:
    priorities = {
        'high': 1,
        'medium': 100,
        'low': 10000,
    }

    def __init__(self, total_floors: int, floor_height: float, speed: float, doors_time: float):
        self.total_floors = total_floors
        self.floor_height = floor_height
        self.speed = speed
        self.doors_time = doors_time
        self.first_floor = 1
        self.cur_floor = self.first_floor
        self.event = asyncio.Event()
        self.queue = []

    def run(self):
        return asyncio.gather(self.wait_input(), self.run_elevator())

    async def wait_input(self):
        stdin, _ = await aioconsole.get_standard_streams()
        async for line in stdin:
            line = line.decode('utf8').strip()
            for value in line.split():
                if value.endswith('o'):
                    value = value[:-1]
                    inside = False
                else:
                    inside = True
                try:
                    floor = int(value)
                except (TypeError, ValueError):
                    pass
                else:
                    if self.first_floor <= floor <= self.total_floors:
                        if inside:
                            if floor >= self.cur_floor:
                                priority = self.get_priority(floor, self.priorities['high'])
                            else:
                                priority = self.get_priority(floor, self.priorities['medium'], reverse=True)
                        else:
                            priority = self.get_priority(floor, self.priorities['low'], reverse=True)
                        item = priority, floor
                        if item not in self.queue:
                            heapq.heappush(self.queue, item)
                            self.event.set()

    async def run_elevator(self):
        while True:
            await self.event.wait()
            self.event.clear()

            while self.queue:
                item = self.queue[0]
                priority, floor = item
                if floor == self.cur_floor:
                    heapq.heappop(self.queue)
                    await self.open_doors()
                else:
                    if floor > self.cur_floor:
                        offset = 1
                    else:
                        offset = -1
                        self.increase_priority(floor)
                    self.cur_floor += offset
                    await asyncio.sleep(self.floor_height / self.speed)
                    print('passes the {} floor'.format(self.cur_floor))

    def get_priority(self, floor, level, reverse=False):
        if reverse:
            p = self.total_floors - floor + 1
        else:
            p = floor
        return p * level

    def increase_priority(self, to_floor):
        queue = []
        for priority, floor in self.queue:
            if priority // self.priorities['low'] and to_floor <= floor < self.cur_floor:
                item = self.get_priority(floor, self.priorities['medium'], reverse=True), floor
            else:
                item = priority, floor
            if item not in queue:
                queue.append(item)
        self.queue = queue
        heapq.heapify(self.queue)

    async def open_doors(self):
        print('opened the doors')
        await asyncio.sleep(self.doors_time)
        print('closed the doors')


def validate_total_floors(value):
    value = int(value)
    min_value, max_value = 5, 20
    if min_value <= value <= max_value:
        return value
    raise argparse.ArgumentTypeError(
        '{} is an invalid value (possible values {}..{})'.format(value, min_value, max_value))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Simulates the elevator.')
    parser.add_argument('total_floors', type=validate_total_floors, help='number of floors')
    parser.add_argument('floor_height', type=float, help='height of one floor, m')
    parser.add_argument('speed', type=float, help='speed of elevator, m/s')
    parser.add_argument('doors_time', type=float, help='time between opening and closing doors, s')
    args = parser.parse_args()

    simulator = ElevatorSimulator(**vars(args))
    loop = asyncio.get_event_loop()
    loop.run_until_complete(simulator.run())
    loop.close()